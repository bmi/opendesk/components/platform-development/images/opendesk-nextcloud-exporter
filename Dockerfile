# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
FROM docker.io/golang:1.21.6-bookworm@sha256:5c7c2c9f1a930f937a539ff66587b6947890079470921d62ef1a6ed24395b4b3 AS build

ARG NEXTCLOUD_EXPORTER_VERSION="v0.6.2"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

WORKDIR /build

RUN wget \
    --quiet \
    "https://github.com/xperimental/nextcloud-exporter/archive/refs/tags/${NEXTCLOUD_EXPORTER_VERSION}.tar.gz" \
    -O "/tmp/exporter.tar.gz"
RUN tar --extract --strip-components=1 --file="/tmp/exporter.tar.gz" --directory "/build" \
 && go mod download \
 && go mod verify

RUN go build -tags netgo -ldflags "-w -X main.Version=${NEXTCLOUD_EXPORTER_VERSION}" -o nextcloud-exporter .

FROM gcr.io/distroless/base-debian12:nonroot@sha256:1a6f64f3fed75597fda0fa9cf8e60c3f6b029dedcef57071a2f42975a1b2bf8a

LABEL org.opencontainers.image.authors="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.documentation=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-exporter/-/blob/main/README.md \
      org.opencontainers.image.source=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-exporter \
      org.opencontainers.image.vendor="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.licenses=Apache-2.0 \
      org.opencontainers.image.base.name=gcr.io/distroless/base-debian12:nonroot \
      org.opencontainers.image.base.digest=sha256:684dee415923cb150793530f7997c96b3cef006c868738a2728597773cf27359

COPY --from=build /build/nextcloud-exporter /bin/nextcloud-exporter

USER nonroot:nonroot
ENV LANG de_DE.UTF-8

CMD ["/bin/nextcloud-exporter"]
