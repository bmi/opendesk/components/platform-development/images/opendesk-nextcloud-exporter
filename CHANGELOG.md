## [1.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-exporter/compare/v1.0.1...v1.0.2) (2024-02-15)


### Bug Fixes

* **Dockerfile:** Update gcr.io/distroless/base-debian12:nonroot Docker digest to 1a6f64f ([fa5f9a0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-exporter/commit/fa5f9a030da309051e72988f60d4f9278fad9be1))

## [1.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-exporter/compare/v1.0.0...v1.0.1) (2024-01-31)


### Bug Fixes

* **Dockerfile:** Update base image to v1.21.6 ([aab48f3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-exporter/commit/aab48f305965f922aa2e40f67634b219422ad00e))

# 1.0.0 (2024-01-03)


### Features

* Initial commit ([5f8c9ce](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-exporter/commit/5f8c9ce59a472655556b1541f9c3ff3e53b89eaf))
